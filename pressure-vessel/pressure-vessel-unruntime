#!/bin/bash

# pressure-vessel-unruntime — undo the Steam Runtime's environment
#
# Copyright © 2017-2019 Collabora Ltd.
#
# SPDX-License-Identifier: MIT
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

set -e
set -o pipefail
set -u
shopt -s nullglob

me="$0"
me="$(readlink -f "$0")"
here="${me%/*}"
me="${me##*/}"

default_path="/usr/local/sbin:/usr/sbin:/sbin:/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games"
steam_runtime="${STEAM_RUNTIME-}"

# Undo any weird environment before we start running external
# executables. We put it back before running the actual app/game.

declare -a options
ld_preload=
options=()

if [ -n "${LD_LIBRARY_PATH-}" ]; then
    options+=("--env-if-host=LD_LIBRARY_PATH=$LD_LIBRARY_PATH")
fi

if [ -n "${LD_PRELOAD-}" ]; then
    options+=("--env-if-host=LD_PRELOAD=$LD_PRELOAD")
    ld_preload="$LD_PRELOAD"
fi

if [ -n "${PATH-}" ]; then
    options+=("--env-if-host=PATH=$PATH")
fi

if [ -n "${STEAM_RUNTIME-}" ]; then
    options+=("--env-if-host=STEAM_RUNTIME=$STEAM_RUNTIME")
fi

unset LD_LIBRARY_PATH
unset LD_PRELOAD
PATH="$default_path"
unset STEAM_RUNTIME

if [ -n "${SYSTEM_LD_LIBRARY_PATH+set}" ]; then
    options+=("--env-if-host=SYSTEM_LD_LIBRARY_PATH=$SYSTEM_LD_LIBRARY_PATH")
    export LD_LIBRARY_PATH="$SYSTEM_LD_LIBRARY_PATH"
fi

if [ -n "${SYSTEM_LD_PRELOAD+set}" ]; then
    options+=("--env-if-host=SYSTEM_LD_PRELOAD=$SYSTEM_LD_PRELOAD")
    export LD_PRELOAD="$SYSTEM_LD_PRELOAD"
fi

if [ -n "${STEAM_RUNTIME_LIBRARY_PATH+set}" ]; then
    options+=("--env-if-host=STEAM_RUNTIME_LIBRARY_PATH=$STEAM_RUNTIME_LIBRARY_PATH")
fi

if [ -n "${SYSTEM_PATH+set}" ]; then
    options+=("--env-if-host=SYSTEM_PATH=$SYSTEM_PATH")
    export PATH="$SYSTEM_PATH"
fi

unset SYSTEM_LD_LIBRARY_PATH
unset SYSTEM_LD_PRELOAD
unset SYSTEM_PATH

old_IFS="$IFS"
IFS=":"
for word in $ld_preload; do
    options+=("--host-ld-preload=$word")
done
IFS="$old_IFS"

for word in "$@"; do
    case "$word" in
        (--batch)
            unset PRESSURE_VESSEL_WRAP_GUI
            ;;
    esac
done

case "${PRESSURE_VESSEL_BATCH-}" in
    (1)
        unset PRESSURE_VESSEL_WRAP_GUI
        ;;
esac

if [ -n "${PRESSURE_VESSEL_WRAP_GUI+set}" ]; then
    if ! result="$("${here}/pressure-vessel-test-ui" --check-gui-dependencies 2>&1)"; then
        result="$(printf '%s' "$result" | sed -e 's/&/\&amp;/' -e 's/</\&lt;/' -e 's/>/\&gt;/')"
        run="env"

        case "$steam_runtime" in
            (/*)
                # Re-enter the Steam Runtime, because STEAM_ZENITY might
                # not work otherwise
                run="$steam_runtime/run.sh"
                ;;
        esac

        exec "$run" "${STEAM_ZENITY:-zenity}" --error --width 500 --text \
            "The pressure-vessel developer/debugging options menu requires Python 3, PyGI, GTK 3, and GTK 3 GObject-Introspection data.

<small>$result</small>"
    fi

    exec "${here}/pressure-vessel-test-ui" ${options+"${options[@]}"} "$@"
fi

exec "$here/pressure-vessel-wrap" ${options+"${options[@]}"} "$@"

# vim:set sw=4 sts=4 et:
