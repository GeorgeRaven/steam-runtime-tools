SPDX-License-Identifier: MIT and LGPL-2.1-or-later and Apache-2.0

The overall license of steam-runtime-tools and pressure-vessel is
LGPL-2.1-or-later <https://spdx.org/licenses/LGPL-2.1-or-later.html>,
which can be found in the file `COPYING.LGPL-2.1`:

    SPDX-License-Identifier: LGPL-2.1-or-later

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library. If not, see <http://www.gnu.org/licenses/>.

Some of the individual source files are under the more permissive
MIT/X11 license, and can be extracted into proprietary projects
without needing to follow the terms of the LGPL:

    Permission is hereby granted, free of charge, to any person obtaining
    a copy of this software and associated documentation files (the
    "Software"), to deal in the Software without restriction, including
    without limitation the rights to use, copy, modify, merge, publish,
    distribute, sublicense, and/or sell copies of the Software, and to
    permit persons to whom the Software is furnished to do so, subject to
    the following conditions:

    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
    MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
    IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
    CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
    TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
    SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

See individual source files for copyright and licensing details, and
in particular pressure-vessel/THIRD-PARTY.md for details of third-party
libraries used or included in pressure-vessel releases.

For supporting code that is marked as being under the
Apache license version 2.0, the full license text can be found in
`COPYING.Apache-2.0`.
